# Participate to Framasoft

## Community-based projects

<!--
  Alphabetical list of Framaservice

  /!\ DO NOT point to folder root but to "index.html"
-->

<div class="clearfix">
  <div class="col-md-3 col-sm-6">
    <a href="framacode/index.html" class="btn btn-lg btn-block btn-default">
      <p><i class="fa fa-2x fa-code" aria-hidden="true"></p>
      <p><b class="violet">Frama</b><b class="jaune">code</b></p>
      <p><b>Development</b></p>
    </a>
  </div>
</div>
