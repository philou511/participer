# Contribuer aux documentations

## Rédaction

Les fichiers sont rédigés avec [la syntaxe markdown](https://docs.framasoft.org/fr/mattermost/help/messaging/formatting-text.html)
mais Gitbook accepte aussi la syntaxe HTML.

Par contre, il n'est pas possible de mixer les 2 syntaxes dans un même bloc.

Si vous voulez ajouter des liens internes, il faut faire attention à pointer vers
les pages `.html` correspondantes au `.md` du dossier.

### Mise en page

La page d'accueil de la doc du Framaprojet peut contenir&nbsp;:

  * une partie présentation
  * prise en main rapide
  * lien vers le logiciel source et une partie présentation du logiciel source (ça peut être issu de la doc officielle)
  * table des matières avec la liste des pages qu'on trouvera dans le sous-menu à gauche.

Les pages de documentation ont la structure&nbsp;:

```
# Titre de la page

## Titre de second niveau

…explications…

### Titre de troisième niveau

…explications…

## Titre de second niveau

…explications…

### Titre de troisième niveau

…explications…
```

Il est préférable de faire des listes (à puces ou numérotées) précisant les étapes plutôt qu'un bloc de texte. Plutôt&nbsp;:

  1. étape 1
  * étape 2
  * étape 3

que&nbsp;:

> Faire étape 1, puis étape 2 et étape 3.

Pour ajouter de la compréhension, il est possible d'ajouter des captures d'écrans (avec flèches et numéros c'est encore mieux). Il est possible de réduire le pods des images en utilisant un service extérieur ou en réduisant la résolution de celles-ci (il est inutile d'utiliser une image de 1920x1600 quand 920x suffit).

Ne pas hésiter à utiliser du gras `**gras**` pour mettre en valeur du texte important ou pour les noms de pages d'un service. Par exemple&nbsp;:

> Cliquez sur **Paramètres** dans le menu

Pour créer des blocs de d'informations il est possible d'utiliser des `div` avec les classes `alert-warning alert` et `alert-info alert` :

```
<div class="alert-warning alert">Attention !</div>
```

<div class="alert-warning alert">Attention !</div>

et

```
<div class="alert-info alert">Information !</div>
```

<div class="alert-info alert">Information !</div>

### Méthode de travail

https://framagit.org/spf/Tutos/blob/master/Git/translate.md
(voir si le tuto est adapté pour la documentation)

  1. Forker
  2. Créer une branche
  3. Faire les changements (commit)
  4. Demander à intégrer les changements (merge request)
  5. Discuter / rectifier

### Structure

Les documentations anglaise et française sont totalement indépendantes.

La documentation de chaque Framaprojet doit être enregistrée dans un dossier
correspondant au nom du logiciel source (exemple : Framacarte → dans le dossier `umap`).

Dans ce dossier, créer un dossier `images` contenant les captures d'écran
et un fichier `README.md` qui contiendra la page d'accueil de présentation du projet

Pour ajouter ensuite le Framaprojet dans la colonne de gauche et sur la page d'accueil,
il faut éditer les fichiers `SUMMARY.md` et `README.md` (à la racine dans le dossier `fr`).

Dans `SUMMARY.md`, il n'est pas possible d'afficher une arborescence
de plus de 2 niveaux et les titres ne sont pas pris en compte
(ils ne sont laissés qu'à titre indicatif).

Chaque lien `.md` trouvé dans ce fichier sera pris en compte par Gitbook
pour créer le fichier `.html` correspondant.
Les fichiers qui ne s'y trouvent pas seront placé dans le dossier
comme de simples fichiers statiques (au même titre que les images)

## Créer une copie locale de la documentation

Pour avoir un aperçu du rendu du site il peut être souhaitable d'installer Gitbook localement.

### Installation de Gitbook

Installez NodeJS, NPM et le client Gitbook :

    sudo apt-get install nodejs-legacy npm
    sudo npm install gitbook-cli -g

### Générer la documentation

Clonez votre fork de la documentation

    git clone https://framagit.org/<utilisateur>/framasoft/docs.git

Depuis le dossier `docs` procédez à l'installation des plugins requis
(la liste des plugins et leur configuration se trouve dans le fichiers `book.json`)

    cd docs
    gitbook init
    gitbook install
    gitbook serve --no-watch

Si tout s'est bien déroulé (la commande se termine par `Serving book on…`,
la copie du site peut être consultée sur :

    http://localhost:4000/

L’option `--no-watch` permet d'éviter que chaque changement dans les
fichiers entraîne un redémarrage automatique du serveur nodejs
ce qui peut être lourd à la longue.
Plus il y a de pages, plus le temps de chargement est long.

Il vaut mieux prendre le temps de finir les changements et redémarrer
le serveur manuellement quant on a fini.

Si vous disposez d'un serveur web en local, plutôt que de se servir du serveur
nodejs, vous pouvez aussi créer le site manuellement avec la commande

    gitbook build

et copier le dossier `_book` où vous le voulez sur le serveur web.
